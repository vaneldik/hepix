#!/bin/csh

if ( $?_HPX_SEEN_HEPIX_CSH ) then
	exit
endif
set _HPX_SEEN_HEPIX_CSH=1

if ( ! $?HPX_INIT ) then
	source /etc/hepix/init.csh
endif

# Top level HEPiX script for C shell derivatives in /etc/profile.d/.

hpx_is_hepix
if ( $status == 0 ) then
	# Current RedHat policy is to source /etc/profile.d/* scripts for every
	# shell, we differentiate what to do based on the shell level.
	# NOTE(fuji): csh(1) does not have $shlvl.
	if ( $?shlvl ) then
		if ( $shlvl == 1 ) then
			hpx_source $HPX_HOME_HEP/csh.login
		else
			hpx_source $HPX_HOME_HEP/csh.cshrc
		endif
	else
		hpx_debug 'No $shlvl available, assuming login shell.'
		hpx_source $HPX_HOME_HEP/csh.login
	endif
endif

# End of file.
