#!/bin/sh

[ -n "$_HPX_SEEN_HEPIX_SH" ] && return
_HPX_SEEN_HEPIX_SH=1

[ -z "$HPX_INIT" ] && . /etc/hepix/init.sh

# Top level HEPiX script for Bourne shell derivatives in /etc/profile.d/.

hpx_is_hepix || return
# Current RedHat policy is to source /etc/profile.d/* scripts for every
# shell, we differentiate what to do based on the shell level.
if [ -n "$SHLVL" ]; then
	if [ $SHLVL = 1 ]; then
		hpx_source $HPX_HOME_HEP/profile
	else
		hpx_source $HPX_HOME_HEP/env
	fi
else
	hpx_debug 'No $SHLVL available, assuming login shell.'
	hpx_source $HPX_HOME_HEP/profile
fi

# End of file.
