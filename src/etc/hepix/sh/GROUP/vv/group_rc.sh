# based on https://its.cern.ch/jira/browse/NOAFS-498

ARCHITECTURE=UNKNOWN; export ARCHITECTURE

# Cern paths check
if [ "$CERN" = "" ]; then
  CERN="/cern"; export CERN
  CERN_LEVEL="pro"; export CERN_LEVEL
  CERN_ROOT="$CERN/pro"; export CERN_ROOT
fi

# Cern paths not set up by default
CERN_LIB="$CERN_ROOT/lib"; export CERN_LIB
CERN_CAR="/cern/old/src/car"; export CERN_CAR
CERN_BIN="$CERN_ROOT/bin"; export CERN_BIN

# CPLEAR environment
 INSTITUTE="CERN"; export INSTITUTE
 CPLEAR="/afs/cern.ch/cplear/offline"; export CPLEAR
 CP_BIN="$CPLEAR/bin"; export CP_BIN
 CP_TEST="$CPLEAR/test"; export CP_TEST
 CP_DEV="$CPLEAR/dev"; export CP_DEV
 CP_NEW="$CPLEAR/pro"; export CP_NEW
 CP_PRO="$CPLEAR/pro"; export CP_PRO
 CP_OLD="$CPLEAR/old"; export CP_OLD
#
 CPLEVEL=$CP_PRO; export CPLEVEL
#
 CP_LIB="$CPLEVEL/lib"; export CP_LIB
 CP_CAR="$CPLEVEL/car; export CP_CAR
 CP_LIS="$CPLEVEL/car; export CP_LIS
 CP_FCA="$CPLEVEL/fca; export CP_FCA
 CP_DAT="$CPLEVEL/dat; export CP_DAT
 CP_ANA="$CPLEAR/ana; export CP_ANA
 CP_MGR="$CPLEAR/mgr; export CP_MGR

# Initialize FATMEN
 FMCPLEAR="/fatmen/fmcplear"; export FMCPLEAR
 FATGRP="cplear"; export FATGRP
 FATPATH="$CPLEAR/kumacs"; export FATPATH

# old scripts set PATH directly but used whitespace as separator. Not working, dropped.
