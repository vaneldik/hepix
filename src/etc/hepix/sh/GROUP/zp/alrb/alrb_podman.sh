
# Instructions are from 
#   https://cern.service-now.com/service-portal?id=kb_article&n=KB0006874

\mkdir -p $HOME/.config/systemd/user/default.target.wants
\mkdir -p $HOME/.config/systemd/user/timers.target.wants

alrb_oldifs="$IFS"
IFS=$'\n'
alrb_lxplusPodmanSetupCmds=(
    "export KRB5CCNAME=FILE:$XDG_RUNTIME_DIR/krb5cc klist"
)

for alrb_podmanCmd in ${alrb_lxplusPodmanSetupCmds[@]}; do
    echo "$alrb_podmanCmd"
    eval "$alrb_podmanCmd"
    if [ $? -ne 0 ]; then
	\echo " Error in lxplus initializations:
You need to join the subordinate-users group and wait 24h for this to work.
  https://e-groups.cern.ch/e-groups/Egroup.do?egroupId=10385897
If you have problems after 24h, please do 'kdestroy -A' and exit.
  Wait a few minuites and then login to lxplus9 to retry.  
Complete CERN podman instructions can be found at 
  https://cern.service-now.com/service-portal?id=kb_article&n=KB0006874 
"
	IFS="$alrb_oldifs"
	return 64
    fi
done

set_ALRB_ContainerEnv ALRB_CONT_RUNTIMEOPT "podman| -v $XDG_RUNTIME_DIR/krb5cc:/tmp/krb5cc_`id -u`:Z -e KRB5CCNAME=/tmp/krb5cc_`id -u`"

IFS="$alrb_oldifs"

return 0


