# CMS-specifics
## https://its.cern.ch/jira/browse/NOAFS-510
## /afs/cern.ch/group/zh/group_env.sh

# This is host dependent. If CvmFS is installed, use it, otherwise fall 
# back on AFS (e.g. on a desktop/laptop w/o CvmFS).
if [ -d /cvmfs/cms.cern.ch ]; then
	CMS_PATH=/cvmfs/cms.cern.ch
else
	CMS_PATH=/afs/cern.ch/cms
fi
export CMS_PATH

# CMS wide settings for latex styles
TEXINPUTS=:${CMS_PATH}/latex/styles ; export TEXINPUTS

# setup for castor
export STAGE_HOST=castorcms

hpx_source $CMS_PATH/cmsset_default.sh
