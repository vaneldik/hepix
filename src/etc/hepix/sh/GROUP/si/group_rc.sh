# https://its.cern.ch/jira/browse/NOAFS-491
#
# SL/AP Group `sys.conf.sh' Environment  --- BAT :-)
#
AP_PROJECT_DIR=/afs/cern.ch/project/slap; export AP_PROJECT_DIR
#
# SL/AP Group Path
#
PATH="$AP_PROJECT_DIR/bin:$AP_PROJECT_DIR/scripts:$PATH"; export PATH
#
if [ -z "$AP_GROUP_CONF" ] ;then
  #
  # SL/AP Manual Path
  #
  if [ -n "$MANPATH" ] ;then
    MANPATH=$AP_PROJECT_DIR/man:$MANPATH; export MANPATH
  else
    MANPATH=$AP_PROJECT_DIR/man; export MANPATH
  fi
  #
  if [ -z "$TEXMFS" ]; then
    TEXMFS="{$AP_PROJECT_DIR/share/texmf,"'!!$TEXMF}' ; export TEXMFS
  fi
  #
  AP_GROUP_CONF=true; export AP_GROUP_CONF
fi
#
# SL/AP HTTPd Server Root Directory
#
AP_WWW_HOME_DIR=$AP_PROJECT_DIR/etc/httpd; export AP_WWW_HOME_DIR
AP_HARVEST_HOME=$AP_PROJECT_DIR/etc/harvest; export AP_HARVEST_HOME
#
# LHC project directory 
#
LHC_DIR=/afs/cern.ch/eng/lhc; export LHC_DIR
