#!/bin/sh

[ -n "$_HPX_SEEN_HEP_ENV" ] && return
_HPX_SEEN_HEP_ENV=1

[ -z "$HPX_INIT" ] && . /etc/hepix/init.sh

hpx_source $HPX_HOME_CORE/pathtools
umask $HPX_UMASK

# Sanitize and set site-wide environment variables.

HPX_INITIALE=`echo $USER | /usr/bin/cut -c1`
export CASTOR_HOME=/castor/cern.ch/user/$HPX_INITIALE/$USER
unset HPX_INITIALE

# $EDITOR is used by SVN, crontab, etc
export EDITOR="/bin/nano -w"

# Determine HEP group name.
HPX_PREF_GROUP=$HPX_USER_HOME/preferred-group
[ -r $HPX_PREF_GROUP ] \
	&& HPX_HEPGROUP=`/bin/cat $HPX_PREF_GROUP` \
	|| HPX_HEPGROUP=`/usr/bin/id -gn`
unset HPX_PREF_GROUP

# Decide whether a group script is packaged and source it;
# then return, unless that script decides otherwise
if [ -r "$HPX_HOME_GROUP/$HPX_HEPGROUP/group_rc.sh" ]; then
    hpx_source "$HPX_HOME_GROUP/$HPX_HEPGROUP/group_rc.sh"

    # $HOME/scripts was used by ~15% of users..
    if [ -d  $HOME/scripts ]; then
	    export PATH="$HOME/scripts:$PATH"
    fi

    # do not source anything else
    return
else
    [ $HPX_VERBOSE -ge 10 ] && hpx_echo "N: No local group script found in $HPX_HOME_GROUP/$HPX_HEPGROUP"
fi

## Continue here only if there's no packaged group script
## in particular PATH manipulation needs to be done in the packaged script

unset HPX_HEPGROUP

set -a
USERPATH=$HOME/scripts
set +a

# Incorporate assembled entries into PATH.
HPX_PATH=$USERPATH

# Append PATH to HPX_PATH and empty PATH, we're going to iterate over
# HPX_PATH and uniquely append to an (initially empty) PATH.
HPX_PATH=${HPX_PATH}:${PATH}
PATH=""

if [ -n "$ZSH_VERSION" ]; then
	for HPX_DIR in ${(s/:/)HPX_PATH}; do
		[ -n "$HPX_DIR" ] && hpx_path_append_unless_present $HPX_DIR
	done
else
	HPX_IFS=$IFS
	IFS=:
	for HPX_DIR in $HPX_PATH; do
		[ -n "$HPX_DIR" ] && hpx_path_append_unless_present $HPX_DIR
	done
	IFS=$HPX_IFS
	unset HPX_IFS
fi
unset HPX_PATH
unset HPX_DIR

unset USERPATH

set +a

hpx_source $HPX_HOME_HEP/rc
_HPX_SHELL=${SHELL##*/}
hpx_source $HPX_HOME_HEP/${_HPX_SHELL}rc
unset _HPX_SHELL

# End of file.
