#!/bin/csh

if ( $?_HPX_SEEN_HEP_CSH_CSHRC ) then
	exit
endif
set _HPX_SEEN_HEP_CSH_CSHRC=1

if ( ! $?HPX_INIT ) then
	source /etc/hepix/init.csh
endif

# NOTE(fuji): sh(1) derivatives source `pathtools' here, but we have to
# define aliases to emulated shell functions for csh(1).  See csh/CORE/init
# for explanation.
alias hpx_path_is_present	"source $HPX_HOME_CORE/hpx_path_is_present.csh "
alias hpx_path_remove		"source $HPX_HOME_CORE/hpx_path_remove.csh "
alias hpx_path_prepend		"source $HPX_HOME_CORE/hpx_path_prepend.csh "
alias hpx_path_prepend_unique	"source $HPX_HOME_CORE/hpx_path_prepend_unique.csh "
alias hpx_path_prepend_unless_present	"source $HPX_HOME_CORE/hpx_path_prepend_unless_present.csh "
alias hpx_path_append		"source $HPX_HOME_CORE/hpx_path_append.csh "
alias hpx_path_append_unique	"source $HPX_HOME_CORE/hpx_path_append_unique.csh "
alias hpx_path_append_unless_present	"source $HPX_HOME_CORE/hpx_path_append_unless_present.csh "

umask $HPX_UMASK

# Sanitize and set site-wide environment variables.

set HPX_INITIALE=`echo $USER | /usr/bin/cut -c1`
setenv CASTOR_HOME /castor/cern.ch/user/$HPX_INITIALE/$USER
unset HPX_INITIALE

# $EDITOR is used by SVN, crontab, etc
setenv EDITOR "/bin/nano -w"

# Determine HEP group name.
set HPX_PREF_GROUP=$HPX_USER_HOME/preferred-group
if ( -r $HPX_PREF_GROUP) then
	set HPX_HEPGROUP=`/bin/cat $HPX_PREF_GROUP`
else
	set HPX_HEPGROUP=`/usr/bin/id -gn`
endif
unset HPX_PREF_GROUP

# Decide whether a group script is packaged and source it;
# then return, unless that script decides otherwise
if ( -r "$HPX_HOME_GROUP/$HPX_HEPGROUP/group_rc.csh" ) then
        hpx_source "$HPX_HOME_GROUP/$HPX_HEPGROUP/group_rc.csh"

	# $HOME/scripts was used by ~15% of users..
	if ( -d $HOME/scripts ) then
		setenv PATH "$HOME/scripts:$PATH"
	endif

        # do not source anything else
	exit
else
	if ( $HPX_VERBOSE >= 10 ) then
		hpx_echo "N: No local group script found in $HPX_HOME_GROUP/$HPX_HEPGROUP"
        endif
endif

## Continue here only if there's no packaged group script
## in particular PATH manipulation needs to be done in the packaged script

unset HPX_HEPGROUP

setenv USERPATH ${HOME}/scripts

# Incorporate assembled entries into PATH.
set HPX_PATH=$USERPATH

# STEP 0. Append PATH to HPX_PATH and empty PATH, we're going
# to iterate over HPX_PATH and uniquely append to an (initially
# empty) PATH.

set HPX_PATH=${HPX_PATH}:${PATH}
setenv PATH ""

# STEP 1. Create a space-separated list from HPX_PATH using the shell's
# PATH<->path synchronization.
set _HPX_PATH_SAVE=$PATH
setenv PATH $HPX_PATH
set hpx_path=($path)
setenv PATH $_HPX_PATH_SAVE
unset _HPX_PATH_SAVE

# STEP 2. Emulate what hpx_path_append_unique() does in sh(1)-derivatives.
# NOTE(fuji): Let's try with our shiny pathtools implementation.
foreach HPX_DIR ($hpx_path)

	eval `hpx_path_append_unless_present "$HPX_DIR"`
end

# STEP 3. Release outer level temporary variables, we're done.
unset HPX_DIR
unset hpx_path
unset HPX_PATH

unset USERPATH

set _HPX_SHELL=${SHELL:t}
hpx_source $HPX_HOME_HEP/${_HPX_SHELL}rc
unset _HPX_SHELL

# End of file.
