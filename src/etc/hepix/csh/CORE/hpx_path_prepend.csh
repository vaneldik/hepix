#!/bin/csh

if ( "$1" == "" ) then
	echo "hpx_path_prepend(): Missing pathname."
	exit
endif

# Prepend given directoy to PATH.
# NOTE(fuji): Caller should eval() the returned result!

echo "setenv PATH ${1}:$PATH"

# End of file.
