#!/bin/csh

if ( "$1" == "" ) then
	echo "hpx_path_remove(): Missing pathname."
	exit
endif

# Remove all occurrences of a given directory from $PATH.
# NOTE(fuji): Caller should eval() the returned result!

set _HPX_PATH=""
set _HPX_PATH_SEP=""

foreach _HPX_DIR ($path)
	if ( "$_HPX_DIR" != "$1" ) then
		set _HPX_PATH=${_HPX_PATH}${_HPX_PATH_SEP}${_HPX_DIR}
	endif
	set _HPX_PATH_SEP=":"
end

echo "setenv PATH $_HPX_PATH"

unset _HPX_PATH
unset _HPX_PATH_SEP

# End of file.
