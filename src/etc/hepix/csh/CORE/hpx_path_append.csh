#!/bin/csh

if ( "$1" == "" ) then
	echo "hpx_path_append(): Missing pathname."
	exit
endif

# Append given directoy to PATH.
# NOTE(fuji): Caller should eval() the returned result!

if ( "$PATH" == "" ) then
	echo "setenv PATH $1"
else
	echo "setenv PATH ${PATH}:${1}"
endif

# End of file.
