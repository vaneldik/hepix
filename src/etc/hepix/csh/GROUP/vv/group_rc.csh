# https://its.cern.ch/jira/browse/NOAFS-498
#
# from /afs/cern.ch/group/vv/group_sys.conf.csh
setenv ARCHITECTURE UNKNOWN

# from /afs/cern.ch/group/vv/group_env.csh
# Cern paths check
if (!($?CERN))then
  setenv CERN  /cern
  setenv CERN_LEVEL pro
  setenv CERN_ROOT  $CERN/pro
endif
# cern paths not set up by default
setenv CERN_LIB  $CERN_ROOT/lib
setenv CERN_CAR  /cern/old/src/car
setenv CERN_BIN  $CERN_ROOT/bin

# CPLEAR environment
setenv INSTITUTE CERN
setenv CPLEAR /afs/cern.ch/cplear/offline
setenv CP_BIN    $CPLEAR/bin
setenv CP_TEST   $CPLEAR/test
setenv CP_DEV    $CPLEAR/dev
setenv CP_NEW    $CPLEAR/pro
setenv CP_PRO    $CPLEAR/pro
setenv CP_OLD    $CPLEAR/old
#
setenv CPLEVEL $CP_PRO

setenv CP_LIB    $CPLEVEL/lib
setenv CP_CAR    $CPLEVEL/car
setenv CP_LIS    $CPLEVEL/car
setenv CP_FCA    $CPLEVEL/fca
setenv CP_DAT    $CPLEVEL/dat
setenv CP_ANA    $CPLEAR/ana
setenv CP_MGR    $CPLEAR/mgr



# Initialize FATMEN
setenv FMCPLEAR /fatmen/fmcplear
setenv FATGRP   cplear
setenv FATPATH $CPLEAR/kumacs

# from /afs/cern.ch/group/vv/group_termset.csh
setenv INITCPLEAR_term yes

alias fatrun "/afs/cern.ch/cplear/offline/fatrun/fatrun.pl"

# from /afs/cern.ch/group/vv/group_login.csh
## cplevel pro  # "source $VV_GROUP_DIR/scripts/cplevel" , no longer exists.

# old scripts set 'path' directly but used whitespace as separator. Not working, dropped.
