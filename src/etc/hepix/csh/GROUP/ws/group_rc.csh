# based on /afs/cern.ch/group/ws/group_env.csh
# modified for migration to cvmfs
setenv OPAL_INSTALL_DIR "/cvmfs/opal.cern.ch"
if ( -f ${OPAL_INSTALL_DIR}/env/group_rc.csh ) then
    echo "Sourcing ${OPAL_INSTALL_DIR}/env/group_rc.csh"
    hpx_source ${OPAL_INSTALL_DIR}/env/group_rc.csh
else
    echo "${OPAL_INSTALL_DIR}/env/group_rc.csh is not available."
    echo "OPAL environment can not be set up."
endif
