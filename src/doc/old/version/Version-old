The following is the History of the HEPiX scripts
project.

The numbereing should be read as: Version.Release.Patch

16 Apr 93: Version 1.0.0
------------------------
	Initial release of the startup scripts
	very preliminary !!! 

	tested in csh, tcsh (csh like scripts)
	tested in sh, bash, ksh, zsh (bourne like scripts)

	on the following machines
	IBM RS6000 under AIX 3.2
	HP 9000/730 under HP-UX 8.07
	SGI 4D/460 under IRIX 4.0.5
	Convex 3210 under ConvexOS 10.1
	Sparcstation 10/30 under SunOS 4.1.3
	DEC 5000/125 under ULTRIX

27 Apr 93: Version 1.0.1
------------------------
	Check termcap file for TERM type

29 Apr 93: Version 1.0.2
------------------------
	zshenv: /etc/kshrc and $HOME/.kshrc get source
		replace aliases by functions
		change PATH for IRIX 5.0
	zprofile: stty command modified for DomainOS
		bug in testing $RUBOUT corrected
	kshrc and .kshrc added
	csh.cshrc: initial system path defined
		prompts changed
		other test for mail in IRIX, DomainOS and HP-UX
		change path for IRIX 5.0
	csh.login: bug for IRIX rlogins corrected	
		stty command modified for DomainOS

07 Jun 93: Version 1.0.3
------------------------
	zshenv: aliases lt and lsize corrected
		SAVEPATH introduced to restore the PATH
		after su
	zprofile: correct bug in setting of TERM 
		switch to application keyboard mode for vt* 
		and xterm*
		handling of DISPLAY variable removed
		see explanation in README.profiles
		resize screen ( bug in HP-UX )
	csh.cshrc: see zshenv
	csh.login: see zprofile

13 Aug 93: Version 1.0.4
------------------------
	zshenv: /usr/etc included in PATH for all users
		new variable LESSCHARSET, which is set to "latin1"
		aliases ls,la,ll,lsize and lt corrected 
	zprofile: stty command modified for all operating systems
		to support 8 bit input/output on tty
		only for HP-UX: eval `resize`
	zshrc:  support completion for new zsh versions
		completions like tcsh
		options extendedglob and recexakt deleted
		options completeinword and appendhistory set 
	csh.cshrc: /usr/etc included in PATH for all users	
		new variable LESSCHARSET, which is set to "latin1"
		new alias pwd to show the user not the mountpoint
		of the automounted filesystem, but the path, which
		is of interest to the user
	csh.login: stty command modified for all operating systems
		to support 8 bit input/output on tty
		only for HP-UX: eval `resize`
	tcshrc: completion modified  ( zcat )	
		completion only for new tcsh versions

09 Feb 94: Version 2.1
----------------------
	HEP_files HEP startup set
		a complete rewrite of setup file
		- first version agreed with CERN -
		There is a file called sys.conf in which you can put all
		the environment variables and it will be parsed from all 
		shells.
		There is also a makefile to install the startup files
		in your home directory for tests or as a global 
		environment.

26 March 94: Version 2.2
------------------------
	improved scan algorithm for sys.conf in the C shell flavour
	no search for $HOME/bin and $HOME/bin.$BINTYPE	
	changed PATH syntax with spaces as seperator
	history file name changed to $HOME/.history.$HOST
	usage of the id instead of the groups command
	new alias name pp instead of psg
	introduction of a separate root search path (ROOTPATH variable in sys.conf) 
	alias rs for resize command

13 April 94: 
------------
	new keybindings and options in zshrc
	. in cdpath (tcshrc,zshrc) added
	improved prompt setting in csh
	prompt setting only in HEP_shenv	
	SH setting in Makefile
            
5 May 94:
---------
	SYSTYPE changed to "sysV" or "bsd"
	fix for the problem  "BASH_VERSION: parameter not set" 
	in scripts with the line #!/bin/ksh -uhf
	additional setting of BINTYPE,SYSTYPE,MANPATH for missing
	sys.conf
	echo message for missing sys.conf only for interactive usage 	
	bugfix in TERM setting (HEP_login )

17 June 94: Version 2.4
-----------------------
	new parsing algorithm for sys.conf
	(now you can evaluate variables like 
		INITIALE=`echo $USER | cut -c1` )
	some changes in termset.[c]sh
            
1 August 94: Version 2.4.1
--------------------------
	Major changements at CERN:
	1 - There are different ways to install the HEP scripts
	  o the scripts can be enforced on a system
	    So some wrappers have been defined.     
	  o the scripts can be available on a system
	    So some central_*.[c]sh files have been designed.
	2 - Not everybody is using the scripts
	    root, adm, all users in /usr/sue/etc/local/list-
	3 - The scripts are now compiled with a debug mode
	  o From a central source repository where the 
	    HEP scripts are a put in a domain, the files are compiled
	    into a debug and an inproduction mode.
	  o The compilation overcomes the ~/bin/whoami bug
	    where whoami is redefines the unix command.
	    All the unix commands are compiled with their absolute
	    name in the HEP scripts.
	  o The debug mode is produced automatically from the instrumented
	    code in the source repository. It shows what files are opened
	    in what order.
	  o The parse algorithm is changed and replaced by a simple source
	    of the sys.conf file which are compiled as well. Thus we don't
	    need the OS extension but the shell flavour extension csh or sh.
	4 - Everything should go with SUE so:
	  - the HEP files should go in /usr/sue/etc instead of /etc
	  - the system adaptation should go in /usr/sue/etc/local
	  - the site customisation level (previously in /etc/local)
	    should go in /usr/sue/etc
	5 - Levels:
	    Need a Site or Model level instead of only a system level:
	       - HEP level
	       - Site or Model level
	       - System level
	       - Group Level 
	       - User level
	    Unfortunately there is no one CERN adaptation but there are 
	    MODELS adaptation (TH, WWW, DXCERN, PDP, L3, etc.) because
	    they are not using AFS fully. This problem will be solved 
	    in 2.4.8.
	6 - Headers:
	    All the headers of files have been standardised in a clean way.
	7 - Files:
	  o startup files (HEP level):
	    - All the files are calling some files in the site or model level.
	    - redefines ENVIRONMENT in HEP_cshenv, HEP_shenv, HEP_login, HEP_profile
	    - all the names will be changed to their actual shell name but with HEP_
	      in front of them at make/compile time:
		HEP_cshenv  -> HEP_csh.cshrc
		HEP_login   -> HEP_csh.login
		HEP_shenv   -> HEP_zshenv
	        HEP_profile -> HEP_zprofile
	  o central files: a lot of specific lines to compensate 
	    - csh defficiencies
	    - ENVIRONMENT unset properly
	    - BATCH mode in xrsh
	  o site customisation or model level files:
	    - all files are calling the system adaptation files but the sys.conf files
	    - zshrc: called by HEP_zshrc at the HEP level. 
	      - unalias pwd and unset HISTFILE
	      - is calling the system adaptation file. unset all the options.
	    - kshrc: includes some lines from Rainer Toebbicke kshrc file
	  o templates: they are very small but growing!

16 August 94: Version 2.4.2
---------------------------
	central templates: gets the today's form. They try to get some informations
		about the shell variables which protect the HEP files. Don't do
		anything if these shell variables are set.
	startup scripts: each of the following files are protected by shell variables
			HEP_cshenv, HEP_shenv, HEP_login, HEP_profile
		So we know if they have been already executed or not. Can't be done twice!
	templates: improved

20 September 94: Version 2.4.3
------------------------------
	Meeting about the location of the scripts:
	 o /usr/sue/etc       -> /usr/local/lib/hepix
	 o /usr/sue/etc/local -> /etc/hepix

	Site customisation: 
	 o PASSWORD_EXPIRES is checked for AFS
	 o coreconf introduced for PDP's adaptation
	 o HPTERM fixed for ksh, tcsh, bash, zsh: can use the arrows as on xterm
	wrappers:
	 o root is excluded fully
	templates:
	 o improved with some text
	 o oracle is added
	tools:
	 o Makefile improved
	 o uco command introduced

27 September 94: Version 2.4.4
------------------------------
	First Introduce In Production on ASIS
	The debug mode is improved. Everything goes into a file /tmp/hepix-log.$$
	Central-Beta is introduced so the scripts can be tested in beta version
	by everybody on ASIS.

	templates:
	 o oracle call is put in oracle_*.[c]sh files; the user has no "hard" line in his
	   template. Miguel Marquina's input.
	wrappers:
	 o users with uid < 100 are not getting the HEPiX environment.
	site customisation:
	 o coreconf is sourced for every body. Will be improved in 2.4.9
	 o A lot of PDP's model is merged with DCI's model.
	tools:
	 o Makefile is improved again
	 

29 September 94: Version 2.4.5
------------------------------
	uco.1 man pages for uco written
	templates are better now and evaluated by Startup_scripts mailing list
	the TERM variable bug is corrected for L3 (and for everybody) as they are
	using the DESY xrlogin to pass the DISPLAY variable.
	the call to the group level is done in all files at the model level.

6 October 94: Version 2.4.6
---------------------------
	o NQS is tested
	  bug fix in C-shells: use the status of the 'tty -s' command instead of
	  the prompt because the shell is started in login mode.
	o Problem with the fact that PDP is using yet another OS definition! So 
	  a '/bin/test -s mail' is called and it is an HP posix-shell call!
	o ENVIRONMENT is reviewed.
	o Documentation for the templates is written.
	
	   

7 October 94: Version 2.4.7
---------------------------
	o uco is in progress
	o the file HEP_kshrc is protected because it can be sourced by .kshrc
	  and it calls this file.
	o all the protections are improved in the same time.

10 October 94: Version 2.4.8
----------------------------
	At least, the sys.conf files can call other levels files so we don't 
	need to support different models but only one for CERN! 
	It clarifies the situation and then we can add a CLUSTER level instead
	of the coreconf file.

18 October 94: Version 2.4.9
----------------------------
	o Implementation of the CLUSTER level. It should increase the efficiency.
	o There is now a /afs/cern.ch/project/hepix volume so there are some
	  modifications for the installation of Beta versions.
	o uco is reviewed to get the '-p shell' option instead of only '-p', etc.

9 November 94: Version 2.4.10
-----------------------------
	o improvement of the zsh user templates
	o improvement of the make.pl and the installation procedure

15 November 94: Version 2.4.11
------------------------------
	o improvement of the make.pl: create a verbose mode
	o introduction of the ftp repository 

21 November 94: Version 2.4.12
------------------------------
	This version had been tested but not put into production it had been 
        skipped in favour of release 2.5
	o last update of the user templates
	o last improvement of the make.pl
	o last improvement of the ftp repository
	o correction to the copyright/motd sequence in login.[c]sh
	o eliminate the 'mail' alias
	o make a clean separation between installation mode 1 and mode 2.
	  In mode 1 don't start anything under /etc now because of the nasty HP!
	o introduce a first beta version of the hepix 2 project with a Xwrapper and 
	  a X-HEP-level domain: first design of a HEP_Xsession.

5 December 94: Version 2.5
--------------------------
	The first in production release of the hepix project in the AFS Cell.


Contact taddei@dxcern.cern.ch

