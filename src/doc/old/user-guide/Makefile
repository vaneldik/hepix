#+#############################################################################
# File:           Makefile
# Description:    Experimental Makefile file to produce the documentation
# Author:         Arnaud Taddei Arnaud.Taddei@cern.ch
#                 Miguel Liebana Miguel.Liebana@cern.ch
# Version:        1.0
# Date:           11/12/1995
#------------------------------------------------------------------------------
# History:
#                 AT 11/12/1995 Creation
#                 AT 12/12/1995 Modifications (including dependencies,
#				mlb's suggestions)
#		  to be done: eps, archive, unarchive, www, etc.
#------------------------------------------------------------------------------
# Full Description:
#       This file should store the receipies to build the following targets:
#          [ps]		- produce a postcript file from a latex file.
#          [html]	- produce an html file from a latex file.
#          [webslide] 	- produce a webslided file from a postscript file.
#          [writeup]	- install a writeup or a reference card.
#          [www]	- install a www page.
#          [gz]		- produce a gzip file.
#          [clean]	- clean the repository from trailing temporary files.
#          [purge]	- purge the repository to only keep the sources files.
#          [archive]	- tar and compress the content of the repository
#                         for archiving removing all the other files.
#          [unarchive]	- uncompress and untar (unarchive) the repository.
#	   [spell]      - starts the spellchecker.
#          [eps]        - produces eps file from a fig file.
#
#       The structure of the document directory should be :
#
#          doc		# where are the latex files, the Makefile, etc.
#            |---fig	# where are the figures: .fig, .eps, .ps, .gif, etc.
#            |---icon	# where are the icons, small figures, etc.
#
#-#############################################################################


#+-------------------
# Document components
#--------------------
DOC= 		user-guide
HTMLTITLE=  	HEPiX Shells and X11 Login Scripts - Implementation at CERN: User Guide
HTMLOPT= 	-split 0 -t "${HTMLTITLE}" -no_navigation -info "" \
		-show_section_numbers
WEBSLIDOPT=     -S 60 -t "${HTMLTITLE}" -cl -1.0 
CHAPTERS= 	${DOC}.tex 
WRITEUP= 	hepixuser
WRITEUP_DIR=    /afs/cern.ch/project/writeups/${WRITEUP}
LANGUAGE=     	english

#+-----------------------
# Implicit building rules
#------------------------
.SUFFIXES :
.SUFFIXES : .tex .dvi .ps

.dvi.ps :
	dvips $< -o $(DOC).ps

.tex.dvi :
	latex $*
	bibtex $*
	latex $*
	latex $*

#+-------------------------------
# General explicit building rules
#--------------------------------

eps :
	echo "Not yet implemented"

spell :	${DOC}.tex
	ispell -t -d ${LANGUAGE} ${DOC}.tex

dvi :   ${DOC}.dvi

ps :	${DOC}.ps

gz :    ${DOC}.ps
	rm -f ${DOC}.ps.gz
	gzip ${DOC}.ps

html :
	latex2html ${HTMLOPT} ${DOC}.tex

webslide : ps
	webslider ${WEBSLIDOPT} ${DOC}.ps

clean :
	rm -f *~ *.dvi *.aux core *.log *.bbl *.toc *.log *.lof *.lop 
	rm -f *.lot *.blg *.los *.glo *.idx
	
purge : clean
	rm -f *~ *.ps *.gz

writeup : ps html
	@(if [ -r "register" ]; then \
	   cp -p register ${WRITEUP_DIR}/register; \
	   cp -p ${DOC}.ps ${WRITEUP_DIR}/main.ps; \
	   if [ -d "${DOC}" ]; then \
	      cp -r ${DOC}/* ${WRITEUP_DIR}; \
	      ln -s -f ${DOC}.html ${WRITEUP_DIR}/main.html; \
	   fi; \
	else \
	   echo "There is no file 'register' in this directory"; \
	   echo "Check http://wwwcn1.cern.ch/writeups/S/Modification.html"; \
	   echo "And get it from:"; \
	   echo "      /afs/cern.ch/project/writeups/Registered/${WRITEUP}"; \
	fi;)

archive :
	echo "Not implemented yet"

unarchive :
	echo "Not implemented yet"


#+-----------------------------
# Local explicit building rules
#------------------------------

www :
	echo "Not implemented yet"


